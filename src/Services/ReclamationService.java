/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.Enseignant;
import Entities.Note;
import Utils.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import Entities.Reclamation;
import Entities.ReclamationNote;
import Entities.ReclamationProf;
import Entities.User;
import Utils.EtatReclamation;
import Utils.UserUniTech;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

/**
 *
 * @author KattaX
 */
public class ReclamationService {

    private Connection con = DataSource.getInstance().getConnection();
    private Statement ste;
    private Note_Service noteService;
    private UserService userService;

    public ReclamationService() {
        try {
            ste = con.createStatement();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        noteService = new Note_Service();
        userService = new UserService();
    }

    public Reclamation rechercherReclamationParID(int id) throws SQLException {
        String req1 = "SELECT * FROM reclamation WHERE id=?";
        PreparedStatement steprep = con.prepareStatement(req1);
        steprep.setInt(1, id);

        Reclamation r = null;
        ResultSet result = steprep.executeQuery();
        if (result.first()) {
            User user = new User();
            user.setId_user(result.getInt("etudiant_id"));
            r = new Reclamation(id, result.getString("description"), user);
            r.setEtat(EtatReclamation.values()[result.getInt("etat")]);
        }
        System.out.println(r);
        return r;
    }

    public boolean existReclamation(int id) throws SQLException {
        return rechercherReclamationParID(id) != null;
    }

    /*  ajout RecAutre */
    public void ajouterReclamation(Reclamation reclamation) throws SQLException {
        /*String req1 = "INSERT INTO `Reclamation` (`description`,`etudiant_id`,sujet,etat) "
                + "VALUES ('" + reclamation.getDesc() + "', '" + reclamation.getUser().getId_user() + "','" + reclamation.getSujet() + "',0);";
         */
        String req = "INSERT INTO `reclamation` (`description`,`etudiant_id`,sujet,etat,dtype, date_creation) VALUES(?,?,?,?,?,?)";
        PreparedStatement ps = con.prepareStatement(req, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, reclamation.getDesc());
        ps.setInt(2, reclamation.getUser().getId_user());

        if (!(reclamation instanceof ReclamationNote || reclamation instanceof ReclamationProf)) {
            ps.setString(3, reclamation.getSujet());
            ps.setString(5, "reclamation");
        } else {
            ps.setString(3, null);
            if (reclamation instanceof ReclamationNote) {
                ps.setString(5, "reclamationnote");
            } else {
                ps.setString(5, "reclamationprof");
            }
        }
        ps.setInt(4, 0);
        Date date1 = new Date();
        String date_now = new SimpleDateFormat("yyyy-MM-dd").format(date1);
        ps.setString(6, date_now);

        ps.executeUpdate();
        ResultSet res = ps.getGeneratedKeys();
        res.next();
        int id_rec = res.getInt(1);
        String codeSuivie = id_rec + "" + getRandomCodeSuivie();
        String reqUpdate = "Update reclamation set code_suivie=? where id=?";
        ps = con.prepareStatement(reqUpdate);
        ps.setString(1, codeSuivie);
        ps.setInt(2, id_rec);
        ps.executeUpdate();

        System.out.println("Reclamation autre ajouté");
    }

    public static int getRandomCodeSuivie() {
        return (int) (Math.random() * ((999 - 100) + 1)) + 100;
    }

    /*  ajout RecNote */
    public void ajouterReclamationNote(ReclamationNote reclamation) throws SQLException {
        /*String req1 = "INSERT INTO `reclamation_Note` (`description`,`etudiant_id`,`id_note`) "
                + "VALUES ('" + reclamation.getDesc() + "', '" + reclamation.getUser().getId_user() + "', '" + reclamation.getId_note() + "');";
        ste.executeUpdate(req1);*/
        ajouterReclamation(reclamation);
        System.out.println("Reclamation Note ajouté");
    }

    /*  ajout RecProf */
    public void ajouterReclamationProf(ReclamationProf reclamation) throws SQLException {
        /*String req1 = "INSERT INTO `reclamation_Prof` (`description`,`etudiant_id`,`id_enseignant`) "
                + "VALUES ('" + reclamation.getDesc() + "', '" + reclamation.getUser().getId_user() + "', '" + reclamation.getProf().getId_user() + "');";
        ste.executeUpdate(req1);*/

        ajouterReclamation(reclamation);
        System.out.println("Reclamation Prof ajouté");

    }

    /*  Modif RecAutre */
    public void modifierReclamation(Reclamation reclamation) throws SQLException {
        String req1 = "UPDATE `reclamation` "
                + "SET `description`=?, etat=? where id=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setString(1, reclamation.getDesc());
        preparedStatement.setInt(2, reclamation.getEtat().ordinal());//ordinal=id
        preparedStatement.setInt(3, reclamation.getId());

        if (preparedStatement.executeUpdate() > 0) {
            System.out.println("Reclamation modifier");
        } else {
            System.out.println("Reclamation non modifier");
        }
    }

    /*  Modif RecNote */
    public void modifierReclamationNote(ReclamationNote reclamation) throws SQLException {
        String req1 = "UPDATE `reclamation_note` "
                + "SET `description`=?, id_note=? , etat=? where id=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setString(1, reclamation.getDesc());
        preparedStatement.setInt(2, reclamation.getNote().getId_note());
        preparedStatement.setInt(3, reclamation.getEtat().ordinal());//ordinal=id
        preparedStatement.setInt(4, reclamation.getId_RecNote());

        if (preparedStatement.executeUpdate() > 0) {
            System.out.println("Reclamation Note modifier");
        } else {
            System.out.println("Reclamation note non modifier");
        }
    }

    /*  Modif RecProf */
    public void modifierReclamationProf(ReclamationProf reclamation) throws SQLException {
        String req1 = "UPDATE `reclamation_prof` "
                + "SET `description`=?, id_enseignant=? , etat=? where id=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setString(1, reclamation.getDesc());
        preparedStatement.setInt(2, reclamation.getProf().getId_user());
        preparedStatement.setInt(3, reclamation.getEtat().ordinal());//ordinal=id
        preparedStatement.setInt(4, reclamation.getId_RecProf());

        if (preparedStatement.executeUpdate() > 0) {
            System.out.println("Reclamation Prof modifier");
        } else {
            System.out.println("Reclamation Prof non modifier");
        }
    }

    public void suprimerReclamation(Reclamation reclamation) throws SQLException {
        String req1 = "DELETE FROM `reclamation` where id=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, reclamation.getId());

        if (preparedStatement.executeUpdate() > 0) {
            System.out.println("Reclamation supprimer");
        } else {
            System.out.println("Reclamation non supprimer");
        }
    }

    public void suprimerReclamationNote(ReclamationNote reclamation) throws SQLException {
        String req1 = "DELETE FROM `reclamation_note` where id=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, reclamation.getId_RecNote());

        if (preparedStatement.executeUpdate() > 0) {
            System.out.println("Reclamation Note supprimer");
        } else {
            System.out.println("Reclamation Note non supprimer");
        }
    }

    public void suprimerReclamationProf(ReclamationProf reclamation) throws SQLException {
        String req1 = "DELETE FROM `reclamation_prof` where id=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, reclamation.getId_RecProf());

        if (preparedStatement.executeUpdate() > 0) {
            System.out.println("Reclamation Prof supprimer");
        } else {
            System.out.println("Reclamation Prof non supprimer");
        }
    }

    public int nbReclamationParType(EtatReclamation etatReclamation) throws SQLException {
        int nb = 0;

        String req1 = "SELECT count(*) FROM `reclamation` where etat=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, etatReclamation.ordinal());

        ResultSet result = preparedStatement.executeQuery();
        if (result.first()) {
            nb = result.getInt(1);
        }

        return nb;
    }

    public int nbReclamationTotal() throws SQLException {
        int nb = 0;

        String req1 = "SELECT count(*) FROM `reclamation`";
        ResultSet result = ste.executeQuery(req1);
        if (result.first()) {
            nb = result.getInt(1);
        }

        System.out.println(nb);
        return nb;

    }

    public int nbReclamationProfTotal() throws SQLException {
        int nb = 0;

        String req1 = "SELECT count(*) FROM `reclamation_prof`";
        ResultSet result = ste.executeQuery(req1);
        if (result.first()) {
            nb = result.getInt(1);
        }
        return nb;

    }

    public int nbReclamationNotesTotal() throws SQLException {
        int nb = 0;

        String req1 = "SELECT count(*) FROM `reclamation_note`";
        ResultSet result = ste.executeQuery(req1);
        if (result.first()) {
            nb = result.getInt(1);
        }
        return nb;

    }

    public double statReclamationParType(EtatReclamation etatReclamation) throws SQLException {
        float res = ((float) nbReclamationParType(etatReclamation) / nbReclamationTotal());
        System.out.println(Double.valueOf(new DecimalFormat("##.##").format(res * 100)) + "%");
        return Double.valueOf(new DecimalFormat("##.##").format(res * 100));
    }

    //jointure
    public Vector<Reclamation> getAll() throws SQLException {

        Vector<Reclamation> reclamations = new Vector<Reclamation>();

        String req1 = "SELECT r.*,user.* FROM reclamation r, user where r.etudiant_id = user.id";
        ResultSet result = ste.executeQuery(req1);
        while (result.next()) {
            User user = new User();
            user.setId_user(result.getInt("etudiant_id"));
            user.setFirst_Name("first_Name");
            Reclamation r = new Reclamation(result.getInt(1), result.getString("description"), user);
            r.setEtat(EtatReclamation.values()[result.getInt("etat")]);

            reclamations.add(r);
        }
        System.out.println(reclamations);
        return reclamations;
    }
    //get all reclamations by userID

    public Vector<Reclamation> getAllParUserID(int id_user) throws SQLException {

        Vector<Reclamation> reclamations = new Vector<Reclamation>();

        String req1 = "SELECT * FROM `reclamation` where etudiant_id=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, id_user);

        ResultSet result = preparedStatement.executeQuery();
        while (result.next()) {
            User user = new User();
            user.setId_user(result.getInt("etudiant_id"));
            Reclamation r = new Reclamation(result.getInt(1), result.getString("description"), user);
            r.setEtat(EtatReclamation.values()[result.getInt("etat")]);

            reclamations.add(r);

        }
        System.out.println(reclamations);
        return reclamations;
    }
    //get all notes by userID

    public Vector<Note> getNoteParUserID(int id_user, String matiere) throws SQLException {

        //noteService.
        Vector<Note> notes = new Vector<Note>();

        String req1 = "SELECT * FROM `note` where id_user=?";
        //   String req1 = "SELECT notes,matiere,type,nom_mat FROM `note` join `matiere` on note.id_mat = matiere.id_mat where id_user=? AND id_mat=?";

        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, id_user);
        //preparedStatement.setString(2, matiere);

        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            int id_note = rs.getInt(1);
            float note_cc = rs.getFloat("note_cc");
            float note_ds = rs.getFloat("note_ds");
            float note_examun = rs.getFloat(4);
            float moyenne = rs.getFloat("moyenne");
            float net = rs.getFloat("net");
            int id_matiere = rs.getInt("id_matiere");
            Note n = new Note(id_note, note_cc, note_ds, note_examun, moyenne, net, id_user, id_matiere);
            notes.add(n);
            /*Note n = new Note(result.getInt(1), result.getInt(3));
            n.setMatiere(Matiere.valueOf(result.getString(4)));
            n.setType(Type_note_matiere.valueOf(result.getString(7)));

            notes.add(n);*/
        }
        //UserUniTech.userConnecte.
        System.out.println(notes);
        return notes;
    }

    public User findUSerById_user(int id_user) throws SQLException {

        User user = new User();
        String req1 = "SELECT * FROM `user` where id=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, id_user);

        ResultSet result = preparedStatement.executeQuery();
        if (result.next()) {
            user.setId_user(result.getInt("id"));
            user.setFirst_Name(result.getString(2));
            user.setLast_Name(result.getString(3));
        }
        return user;
    }

    public Enseignant findEnsById_user(int id_user) throws SQLException {

        Enseignant enseignant = new Enseignant();

        String req1 = "SELECT * FROM `user` where id_user=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, id_user);

        ResultSet result = preparedStatement.executeQuery();
        if (result.next()) {
            enseignant.setId_user(result.getInt("id_user"));
            enseignant.setFirst_Name(result.getString(2));
            enseignant.setLast_Name(result.getString(3));
        }

        return enseignant;
    }

    //get all prof by userID
    public Vector<User> getListEnseignantsClasse(int id_classe) throws SQLException {

        Vector<User> enseignants = new Vector<User>();

        String req1 = "SELECT * FROM `classeenseignant` where id_classe=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, id_classe);

        ResultSet result = preparedStatement.executeQuery();
        while (result.next()) {
            User user = findUSerById_user(result.getInt("id_enseignant"));
            enseignants.add(user);

        }

        return enseignants;
    }

    public Vector<User> getUsersByRoleAndClasse(String role, String classe) throws SQLException {

        Vector<User> users = new Vector<User>();

        String req1 = "SELECT * FROM `user` where role=? AND classe=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setString(1, role);
        preparedStatement.setString(2, classe);

        ResultSet result = preparedStatement.executeQuery();
        while (result.next()) {
            User user = new User();
            user.setId_user(result.getInt("id_user"));
            user.setFirst_Name(result.getString("first_Name"));
            user.setLast_Name(result.getString("last_Name"));
            user.setEmail(result.getString("email"));
            user.setImage_user(result.getString("image_user"));
            user.setClasse(result.getString("classe"));
            users.add(user);
        }

        return users;
    }

    public String getTypeUserByID(int idUser) throws SQLException {

        String req1 = "SELECT * FROM `user` where id=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, idUser);

        ResultSet result = preparedStatement.executeQuery();
        while (result.next()) {
            return result.getString("dtype");
        }

        return "user";

    }

    public String getMatierByID(int idMatiere) throws SQLException {

        String req1 = "SELECT nom_mat FROM `matiere` where id_matiere=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, idMatiere);

        ResultSet result = preparedStatement.executeQuery();
        if (result.next()) {
            return result.getString(1);
        }

        return "test";

    }

    public Vector<Note> getNoteByUserID(int idUser) throws SQLException {
        Vector<Note> notes = new Vector<>();

        String req1;
        PreparedStatement preparedStatement;

        if (getTypeUserByID(idUser).equals("admin")) {
            req1 = "SELECT * FROM `note`,user join id_user on user.id_user";
            preparedStatement = con.prepareStatement(req1);
        } else {
            req1 = "SELECT * FROM `note`,user join id_user on user.id_user where id_user=?";
            preparedStatement = con.prepareStatement(req1);
            preparedStatement.setInt(1, idUser);
        }

        //   String req1 = "SELECT notes,matiere,type,nom_mat FROM `note` join `matiere` on note.id_mat = matiere.id_mat where id_user=? AND id_mat=?";
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            //Note n = new Note(result.getInt(1), result.getInt(3));
            int id_note = rs.getInt(1);
            float note_cc = rs.getFloat("note_cc");
            float note_ds = rs.getFloat("note_ds");
            float note_examun = rs.getFloat(4);
            float moyenne = rs.getFloat("moyenne");
            float net = rs.getFloat("net");
            int id_user = rs.getInt("id_user");
            int id_matiere = rs.getInt(8);
            Note n = new Note(id_note, note_cc, note_ds, note_examun, moyenne, net, id_user, id_matiere);

            notes.add(n);
        }
        return notes;
    }

    public Note getNoteByIDNote(int idNote) throws SQLException {

        String req1 = "SELECT * FROM `note` where id_note=?";
        PreparedStatement preparedStatement = con.prepareStatement(req1);
        preparedStatement.setInt(1, idNote);

        Note note = null;

        ResultSet rs = preparedStatement.executeQuery();
        /*while (result.next()) {
            note = new Note(idNote, result.getInt(3));
        }*/
        if (rs.next()) {
            int id_note = rs.getInt(1);
            float note_cc = rs.getFloat("note_cc");
            float note_ds = rs.getFloat("note_ds");
            float note_examun = rs.getFloat(4);
            float moyenne = rs.getFloat("moyenne");
            float net = rs.getFloat("net");
            int id_user = rs.getInt("id_user");
            int id_matiere = rs.getInt(8);
            note = new Note(id_note, note_cc, note_ds, note_examun, moyenne, net, id_user, id_matiere);
        }
        return note;
    }

    public Vector getReclamationsByUserID(int idUser, String typeRec) throws SQLException {
        Vector reclamations = null;
        String req1;
        PreparedStatement preparedStatement = null;

        if (typeRec.equalsIgnoreCase("Notes")) {
            if (getTypeUserByID(idUser).equals("admin")) {
                req1 = "SELECT * FROM `reclamation` where dtype='reclamationnote'";
                preparedStatement = con.prepareStatement(req1);
            } else {
                req1 = "SELECT * FROM `reclamation` where etudiant_id=? and dtype='reclamationnote'";
                preparedStatement = con.prepareStatement(req1);
                preparedStatement.setInt(1, idUser);
            }
            reclamations = new Vector<ReclamationNote>();
        } else if (typeRec.equalsIgnoreCase("Enseignants")) {
            if (getTypeUserByID(idUser).equals("admin")) {
                req1 = "SELECT * FROM `reclamation` r where r.dtype='reclamationprof'";
                preparedStatement = con.prepareStatement(req1);
            } else {
                req1 = "SELECT * FROM `reclamation` r where r.etudiant_id=? and r.dtype='reclamationprof'";
                preparedStatement = con.prepareStatement(req1);
                preparedStatement.setInt(1, idUser);
            }
            reclamations = new Vector<ReclamationProf>();
        } else {
            if (getTypeUserByID(idUser).equals("admin")) {
                req1 = "SELECT * FROM `reclamation`";
                preparedStatement = con.prepareStatement(req1);
            } else {
                req1 = "SELECT * FROM `reclamation` where etudiant_id=?";
                preparedStatement = con.prepareStatement(req1);
                preparedStatement.setInt(1, idUser);
            }
            reclamations = new Vector<Reclamation>();
        }

        //   String req1 = "SELECT notes,matiere,type,nom_mat FROM `note` join `matiere` on note.id_mat = matiere.id_mat where id_user=? AND id_mat=?";
        ResultSet result = preparedStatement.executeQuery();
        while (result.next()) {

            if (typeRec.equalsIgnoreCase("Notes")) {

                Note n = getNoteByIDNote(result.getInt(4));

                ReclamationNote r = new ReclamationNote(result.getInt(1), result.getString(2), findUSerById_user(result.getInt("etudiant_id")), n, EtatReclamation.values()[result.getInt(5)]);

                reclamations.add(r);

            } else if (typeRec.equalsIgnoreCase("Enseignants")) {

                ReclamationProf r = new ReclamationProf(result.getInt(1), findUSerById_user(result.getInt("etudiat_id")), result.getString(2), EtatReclamation.values()[result.getInt(5)], findEnsById_user(result.getInt(4)));
                reclamations.add(r);
            } else {
                Reclamation r = new Reclamation(result.getInt(1), result.getString("description"), findUSerById_user(result.getInt("etudiant_id")));
                r.setEtat(EtatReclamation.values()[result.getInt("etat")]);

                reclamations.add(r);
            }
        }

        System.out.println("reclamations: " + reclamations);

        return reclamations;
    }

}
