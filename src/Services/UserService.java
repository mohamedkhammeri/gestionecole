/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entities.Administrateur;
import Entities.Enseignant;
import Entities.Etudiant;
import Entities.Parent;
import Entities.User;
import Utils.DataSource;
import Utils.org.mindrot.jbcrypt.BCrypt;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserService {

    private Connection con = DataSource.getInstance().getConnection();
    private Statement ste;
    Etudiant e = new Etudiant();

    public UserService() {
        try {
            ste = con.createStatement();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public void ajouterUser(User user) throws SQLException {
        Date date1 = new Date();
        String account_Date = new SimpleDateFormat("yyyy-MM-dd").format(date1);
        user.setAccount_Date(account_Date);

        String req1 = "INSERT INTO user(`first_Name`,`last_Name`,`username`,`password`,"
                + "`email` , `phone_number`, `CIN`, `username_canonical`,"
                + "`email_canonical`,`enabled`,`salt`,`dtype`,roles) "
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement ps = con.prepareStatement(req1, Statement.RETURN_GENERATED_KEYS);

        user.setSalt(BCrypt.gensalt());

        ps.setString(1, user.getFirst_Name());
        ps.setString(2, user.getLast_Name());
        ps.setString(3, user.getUser_Name());
        ps.setString(4, BCrypt.hashpw(user.getPassword(), user.getSalt()));
        ps.setString(5, user.getEmail());
        ps.setInt(6, user.getPhone_number());
        ps.setInt(7, user.getCIN());
        ps.setString(8, user.getUser_Name());
        ps.setString(9, user.getEmail());
        ps.setBoolean(10, true);
        ps.setString(11, user.getSalt());
        if (user.getRole().equalsIgnoreCase("parent") || user.getRole().equalsIgnoreCase("admin")) {
            ps.setString(12, "user");
            if (user.getRole().equalsIgnoreCase("admin")) {
                ps.setString(13, "a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}");
            } else {
                ps.setString(13, "a:0:{}");
            }
        } else {
            ps.setString(12, user.getRole());
            if (user.getRole().equalsIgnoreCase("etudiant")) {
                ps.setString(13, "a:1:{i:0;s:12:\"ROLE_STUDENT\";}");
            } else if (user.getRole().equalsIgnoreCase("enseignant")) {
                ps.setString(13, "a:1:{i:0;s:8:\"ROLE_ENS\";}");
            } else {
                ps.setString(13, "a:0:{}");
            }
        }

        ps.executeUpdate();

        ResultSet res = ps.getGeneratedKeys();
        res.next();
        int id_user = res.getInt(1);
        String req = "";
        if (user.getRole().equalsIgnoreCase("etudiant")) {
            req = "INSERT INTO etudiant(id) values(?)";
            ps = con.prepareStatement(req);
            //ps.setInt(2, user.getIdClasse());
        } else {
            req = "INSERT INTO enseignant values(?)";
            ps = con.prepareStatement(req);
        }

        ps.setInt(1, id_user);

        ps.executeUpdate();

        System.out.println("Utilisateur ajouté");

    }

    public void modifierUser(User u) throws SQLException {
        String sql = "UPDATE user SET `first_Name`=?,`last_Name`=?,`username`=?,`password`=?, `email`=?, "
                + "`phone_number`=?, `gender`=?, `CIN`=?, `account_Date`=?, `Image_user`=?, `age`=?, `classe`=? , `level`=? , `usernameCanonical`=? , `emailCanonical`=?  , `plainPassword`=? , `role`=? WHERE id_user=" + u.getId_user();
        PreparedStatement ste;
        try {
            ste = con.prepareStatement(sql);

            ste.setString(1, u.getFirst_Name());

            ste.setString(2, u.getLast_Name());
            ste.setString(3, u.getUser_Name());
            ste.setString(4, u.getPassword());
            ste.setString(5, u.getEmail());
            ste.setInt(6, u.getPhone_number());
            ste.setString(7, u.getGender());
            ste.setInt(8, u.getCIN());
            ste.setString(9, u.getAccount_Date());
            ste.setString(10, u.getImage_user());
            ste.setInt(11, u.getAge());

            ste.setString(12, u.getClasse());
            ste.setString(13, u.getLevel().toString());
            ste.setString(14, u.getUsernameCanonical());
            ste.setString(15, u.getEmailCanonical());

            ste.setString(16, u.getPlainPassword());

            ste.setString(17, u.getRole());

            ste.executeUpdate();
            int rowsUpdated = ste.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("La modification de l'utilisateur : " + u.getFirst_Name() + " a été éffectuée avec succès ");
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void supprimerUser(User u) {

        try {
            String req = "DELETE FROM `user` WHERE `user`.`id_user` = ?";
            PreparedStatement ste = con.prepareStatement(req);
            ste.setInt(1, u.getId_user());
            ste.executeUpdate();
            System.out.println("Utilisateur supprimé");

        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<User> readAll() throws SQLException {
        List<User> list = new ArrayList<>();
        ResultSet res = ste.executeQuery("select * from user ");
        User u = null;
        while (res.next()) {
            u = new User(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getInt(7), res.getString(8), res.getInt(9), res.getString(10), res.getString(11), res.getInt(12), res.getString(13));
            u.setLevel(Entities.Level.valueOf(res.getString(14)));
            u.setRole(res.getString(21));
            list.add(u);
            System.out.println("all" + list);
        }
        return list;
    }

    public String FindNomUserByIdUser(int id_user) throws SQLException {
        String req = "SELECT * from user where Id_User='" + id_user + "'";
        ResultSet res = ste.executeQuery(req);

        String nom = null;
        while (res.next()) {

            nom = res.getString(2);
            System.out.println(nom);
        }
        return nom;
    }

    public List<User> TrierParDateCreation() throws SQLException {
        List<User> list = new ArrayList<>();
        ResultSet res = ste.executeQuery("select * from user ORDER BY account_Date ASC");
        User com = null;
        while (res.next()) {
            com = new User(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getInt(7), res.getString(8), res.getInt(9), res.getString(10), res.getString(11), res.getInt(12), res.getString(13));
            list.add(com);
        }
        System.out.println(list);
        return list;
    }

    public String getUserRole(int id_user) throws SQLException {
        String req = "SELECT dtype from user where id='" + id_user + "'";
        ResultSet res = ste.executeQuery(req);
        String role = null;

        while (res.next()) {
            role = res.getString("dtype");
        }

        return role;
    }

    public User getUserById(int id_user) throws SQLException {
        User user = new User();
        Etudiant etudiant = new Etudiant();
        Enseignant enseignant = new Enseignant();
        Parent parent = new Parent();
        if (getUserRole(id_user) == "etudiant") {
            String req = "SELECT * from user where id='" + id_user + "'";
            ResultSet res = ste.executeQuery(req);
            while (res.next()) {
                etudiant.setId_user(res.getInt("id"));
                etudiant.setFirst_Name(res.getString("first_Name"));
                etudiant.setLast_Name(res.getString("last_Name"));
                etudiant.setUser_Name(res.getString("username"));
                etudiant.setPassword(res.getString("password"));
                etudiant.setEmail(res.getString("email"));
                etudiant.setPhone_number(res.getInt("phone_number"));
                //etudiant.setGender(res.getString("gender"));
                etudiant.setCIN(res.getInt("CIN"));
                //etudiant.setAccount_Date(res.getString("account_Date"));
                //etudiant.setImage_user(res.getString("Image_user"));
                //etudiant.setAge(res.getInt("age"));
                //etudiant.setClasse(res.getString("classe"));
                //etudiant.setIdClasse(res.getInt("id_classe"));
                etudiant.setRole(res.getString("dtype"));
            }
            user = etudiant;
        }
        if (getUserRole(id_user) == "enseignant") {
            String req = "SELECT * from user where id_user='" + id_user + "'";
            ResultSet res = ste.executeQuery(req);
            while (res.next()) {
                enseignant.setId_user(res.getInt("id"));
                enseignant.setFirst_Name(res.getString("first_Name"));
                enseignant.setLast_Name(res.getString("last_Name"));
                enseignant.setUser_Name(res.getString("username"));
                enseignant.setPassword(res.getString("password"));
                enseignant.setEmail(res.getString("email"));
                enseignant.setPhone_number(res.getInt("phone_number"));
                //enseignant.setGender(res.getString("gender"));
                enseignant.setCIN(res.getInt("CIN"));
                //enseignant.setAccount_Date(res.getString("account_Date"));
                //enseignant.setImage_user(res.getString("Image_user"));
                //enseignant.setAge(res.getInt("age"));
                //enseignant.setClasse(res.getString("classe"));
                enseignant.setRole(res.getString("dtype"));
            }
            user = enseignant;
        }
        if (getUserRole(id_user) == "parent") {
            String req = "SELECT * from user where id_user='" + id_user + "'";
            ResultSet res = ste.executeQuery(req);
            while (res.next()) {
                parent.setId_user(res.getInt("id_user"));
                parent.setFirst_Name(res.getString("first_Name"));
                parent.setLast_Name(res.getString("last_Name"));
                parent.setUser_Name(res.getString("username"));
                parent.setPassword(res.getString("password"));
                parent.setEmail(res.getString("email"));
                parent.setPhone_number(res.getInt("phone_number"));
                parent.setGender(res.getString("gender"));
                parent.setCIN(res.getInt("CIN"));
                parent.setAccount_Date(res.getString("account_Date"));
                parent.setImage_user(res.getString("Image_user"));
                parent.setAge(res.getInt("age"));
                parent.setRole(res.getString("Role"));
            }
            user = parent;
        }
        System.out.println(user);
        return user;
    }

    public Boolean FindUNameByUName(String user_Name) throws SQLException {
        String req = "SELECT * from user where username='" + user_Name + "'";
        ResultSet res = ste.executeQuery(req);
        User com = null;
        String User_Name = null;
        while (res.next()) {
            User_Name = res.getString(4);
            if (user_Name.equals(User_Name)) {
                return true;

            }

        }

        return false;
    }

    public List<User> readAllUserByLevel(Entities.Level level) throws SQLException {
        List<User> list = new ArrayList<>();
        ResultSet res = ste.executeQuery("select * from user WHERE level='" + level + "'AND classe='" + null + "'");
        User u = null;
        while (res.next()) {
            u = new User(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getInt(7), res.getString(8), res.getInt(9), res.getString(10), res.getString(11), res.getInt(12), res.getString(13));
            u.setLevel(Entities.Level.valueOf(res.getString(14)));
            u.setRole(res.getString(21));
            list.add(u);
        }
        System.out.println("all" + list);

        return list;
    }

    public User FindUserByFirstAndLastName(String first, String last) throws SQLException {
        String req = "SELECT * from user where first_Name='" + first + "'AND last_Name='" + last + "'";
        ResultSet res = ste.executeQuery(req);
        User etudiant = new User();

        while (res.next()) {
            etudiant.setId_user(res.getInt("id_user"));
            etudiant.setFirst_Name(res.getString("first_Name"));
            etudiant.setLast_Name(res.getString("last_Name"));
            etudiant.setUser_Name(res.getString("username"));
            etudiant.setPassword(res.getString("password"));
            etudiant.setEmail(res.getString("email"));
            etudiant.setPhone_number(res.getInt("phone_number"));
            etudiant.setGender(res.getString("gender"));
            etudiant.setCIN(res.getInt("CIN"));
            etudiant.setAccount_Date(res.getString("account_Date"));
            etudiant.setImage_user(res.getString("Image_user"));
            etudiant.setAge(res.getInt("age"));
            etudiant.setLevel(Entities.Level.valueOf(res.getString("level")));
            etudiant.setClasse(res.getString("classe"));
            etudiant.setRole(res.getString("role"));
        }
        return etudiant;
    }

    public User FindUserByLoginAndPassword(String login, String password) throws SQLException {
        String req = "SELECT * from user where username='" + login + "'AND password='" + password + "'";
        ResultSet res = ste.executeQuery(req);
        User etudiant = new User();

        while (res.next()) {
            etudiant.setId_user(res.getInt("id_user"));
            etudiant.setFirst_Name(res.getString("first_Name"));
            etudiant.setLast_Name(res.getString("last_Name"));
            etudiant.setUser_Name(res.getString("username"));
            etudiant.setPassword(res.getString("password"));
            etudiant.setEmail(res.getString("email"));
            etudiant.setPhone_number(res.getInt("phone_number"));
            etudiant.setGender(res.getString("gender"));
            etudiant.setCIN(res.getInt("CIN"));
            etudiant.setAccount_Date(res.getString("account_Date"));
            etudiant.setImage_user(res.getString("Image_user"));
            etudiant.setAge(res.getInt("age"));
            etudiant.setLevel(Entities.Level.valueOf(res.getString("level")));
            etudiant.setClasse(res.getString("classe"));
            etudiant.setRole(res.getString("role"));
        }
        return etudiant;
    }

    public User authentifier(String username, String pwd) throws SQLException {
        String req = "select * from user WHERE username=?";
        PreparedStatement steprep = con.prepareStatement(req);
        steprep.setString(1, username);

        ResultSet res = steprep.executeQuery();

        User u = null;
        if (res.next()) {
            if (BCrypt.checkpw(pwd, res.getString("password"))) {
                //u = new User(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getInt(7), res.getString(8), res.getInt(9), res.getString(10), res.getString(11), res.getInt(12), res.getString(13));
                //u.setLevel(Entities.Level.valueOf(res.getString(14)));

                if (res.getString("dtype").equalsIgnoreCase("etudiant")) {
                    u = new Etudiant();
                } else if (res.getString("dtype").equalsIgnoreCase("enseignant")) {
                    u = new Enseignant();
                } else {
                    u = new User();
                }
                
                if (res.getString("roles").contains("ROLE_SUPER_ADMIN")) {
                    u.setRole("admin");
                } else {
                    u.setRole(res.getString("dtype"));
                }
                u.setCIN(res.getInt("CIN"));
                u.setPhone_number(res.getInt("phone_number"));
                u.setUser_Name(res.getString("username"));
                u.setEmail(res.getString("email"));
                u.setFirst_Name(res.getString("first_Name"));
                u.setLast_Name(res.getString("last_Name"));
                u.setId_user(res.getInt("id"));
            }
        }

        return u;
    }
}
